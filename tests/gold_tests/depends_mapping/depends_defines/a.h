#ifndef __SAMPLES_CPPDEFINES__A_H__
#define __SAMPLES_CPPDEFINES__A_H__

#if !defined(SAMPLES_CPPDEFINE)
#error "SAMPLES_CPPDEFINE is not defined"
#endif

#endif // __SAMPLES_CPPDEFINES__A_H__
/* vim: set et ts=4 sw=4 ai : */
