#include <stdio.h>

#include <symlinks1/symlinks1.h>
#include <symlinks3/symlinks3.h>

int main(int argc, char** argv)
{
    symlinks1();
    symlinks3();

    return 0;
}

/* vim: set et ts=4 sw=4 ai : */

